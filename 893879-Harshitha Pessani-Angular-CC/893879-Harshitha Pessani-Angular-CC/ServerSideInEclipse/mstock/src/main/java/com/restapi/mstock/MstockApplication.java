package com.restapi.mstock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MstockApplication {

	public static void main(String[] args) {
		SpringApplication.run(MstockApplication.class, args);
	}

}
