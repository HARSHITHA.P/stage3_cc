package com.restapi.mstock.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restapi.mstock.dao.CompanyStockDao;
import com.restapi.mstock.models.CompanyDetails;

@Service
public class CompanyStockServices {

	@Autowired
	private CompanyStockDao companyDao;

	public List<CompanyDetails> getCompanyStockDetails() {

		return companyDao.findAll();
	}
	
	
}
