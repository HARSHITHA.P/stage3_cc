package com.restapi.mstock.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restapi.mstock.dao.StockDao;
import com.restapi.mstock.models.Stock;

@Service
public class StockServices {

	@Autowired
	StockDao dao;
	
	public List<Stock> getstockcompare(String company,String fromdate,String todate){

		return dao.getstockcompare(company,fromdate,todate);
		}
	
}

